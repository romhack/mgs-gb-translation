fillRamwith_e_d_times: EQU $5843
copyTenCharString: EQU $4e99
HW_WRAM_BANK_NUM: EQU $ff70
nullify_and_ret: EQU $10e0
ramBuffer: EQU $D800

	ld      e, $50 ; full space tile
	ld      d, 4        
	call    fillRamwith_e_d_times
	
	ld      a, [HW_WRAM_BANK_NUM]
	push    af          
	ld      a, 2        
	ld      [HW_WRAM_BANK_NUM], a
	ld      de, continueString
	ld      hl, ramBuffer  
	call    copyTenCharString
	
	ld      de, retryString
	ld      hl, ramBuffer+$10
	call    copyTenCharString
	
	ld 	de, exitString 
	ld 	hl, ramBuffer+$20
	call	copyTenCharString
	
	pop     af          
	ld      [HW_WRAM_BANK_NUM], a
	ld      a, [$C447]
	ld      [$C445], a
	ld      a, $18      
	ld      [$C447], a
	jp      nullify_and_ret; original exit

continueString:;strings for RETRY box draw:
DB  $50,$50,$50,$50,$70,$71,$6F,$64,$6F,$6C,$67,$69,$73,$7D,$FF;����������
retryString:
DB  $50,$50,$50,$50,$68,$60,$6e,$6f,$62,$6f,$FF;������
exitString:
DB  $50,$50,$50,$50,$62,$7c,$6a,$73,$69,$FF;�����