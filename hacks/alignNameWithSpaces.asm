	;b has count of leftover spaces to align
	;de - dstPtr
	push bc ;c has name number already shifted

	ld      b, 0 
	ld      hl, ALIGN_STR_PTR_TBL
	add 	hl,	bc; get ptr to name's align string
	ldi	a, [hl]
	ld	h, [hl]		; load 16 bit ptr from RAM
	ld	l, a	;set read postion on string start
	pop bc
.loop:
	ldi 	a, [hl]
	ld      [de], a         ; save to dst align char from str
	inc     de              
	dec     b               
	jr      nz, .loop
	
	jp 	$4ce3 		;also proceed to usual actions	

ALIGN_STR_PTR_TBL:
DW SNAKE, CAMPBELL, MEI_LING, WEASEL, MC_BRIDE, CRIS, NO4, JENNER, HAWK

;50 - full space; 51 - zero-width space for alignment
SNAKE: ; not used
CAMPBELL: DB 0x2F, 0x2F; Кэмпбелл + 2
MEI_LING: DB 0x50, 0x51; Мей Линг + 2
WEASEL: DB 0x50, 0x2F, 0x2F, 0x2F; Визель + 4
MC_BRIDE: DB 0x2F, 0x50, 0x51; МакБрид +3
CRIS: DB 0x2F, 0x51, 0x51, 0x50, 0x50, 0x50; Крис + 6
NO4: DB 0x2F, 0x2F, 0x50; Номер 4 + 3
JENNER: DB 0x2F, 0x2F
HAWK: DB 0x2F, 0x2F


