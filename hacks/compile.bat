@echo off
set tniasm="..\Tools\tniasm\tniasm.exe"
:start
copy "Metal Gear Solid (script patch).gbc" "Metal Gear Solid (VWF Hack) [C][!].gbc"
echo BACKUP RESTORED
%tniasm% main.asm
IF EXIST tniasm.out DEL /Q tniasm.out >NUL
IF EXIST tniasm.tmp DEL /Q tniasm.tmp >NUL

echo COMPILE AGAIN?
pause
cls
goto start