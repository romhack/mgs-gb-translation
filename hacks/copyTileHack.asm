; a has char index
					; renderString+12p ...
		ld 	e, a		;save for width load
		ld	c, a
		swap	a
		and	$F		; a has	hinybble in low
		ld	b, a
		ld	a, c		; restore index
		swap	a
		and	$F0 ; '�'       ; now bc is shifted left 4
		srl	b
		rra			; 16 bit shiftr, so now	it's shifted left 3
		ld	c, a		; bc = a*8 (16 bit)

		ldh	a, [SVBK_WRAM_Bank]
		push	af		; save current wram bank number
		ld	a, 7		; last WRAM bank has outBuffer
		ldh	[SVBK_WRAM_Bank], a

		ld 	hl,WIDTH_TBL
		ld 	d,0
		add 	hl,de;points to current width
		ld 	a,[hl];read width
		ld 	[CHAR_WIDTH],a

		ld	hl, SrcFontPtr
		ldi	a, [hl]
		ld	h, [hl]		; load 16 bit ptr from RAM
		ld	l, a		; hl points to font start in ROM
		ld	a, [LangId]
		bit	7, a
		jr	z, .skipFontAddrUpdate ; bc has	offset of char in font tbl
		ld	hl, ChineseFont

.skipFontAddrUpdate:			
		add	hl, bc		; bc has offset	of char	in font	tbl, hl points to char gfx
		ld	a, l		;save ptr to gfx
		ld	[SRC_CHAR_PTR],	a
		ld	a, h
		ld	[SRC_CHAR_PTR+1], a 

		ld	e, l
		ld	d, h		; put ptr to char tile in de for LOAD_CHAR_TILE

		call 	LOAD_CHAR_TILE

		ld 	a, [POS]
		ld 	c, a		;c=pos
		call 	CHAR_TILE_SHR_C
		call 	OUT_OR_CHAR
		call 	COPY_OUT_TILE

		ld d, h
		ld e, l ;save final pos in outBuffer for future save

		ld 	a, [POS]
		ld 	c, a
		cpl			; negate a
		add 	a, 9
		ld 	[EMPTY],a	;empty=8-pos
	
		ld 	a, c		;a = pos
		ld 	hl,CHAR_WIDTH
		add 	a,[hl]
		ld 	[POS],a		;pos=pos+charwidth
		cp 8
		jr c, .popNLoop		;if pos<8, read next
		;if pos is >= 8, dump two tiles:

		ld	a, e		;save pos in outBuffer
		ld	[OutBufferPtr],	a
		ld	a, d
		ld	[OutBufferPtr+1], a ; 16 bit save outbuffer position back
		call 	CLEAR_OUT_TILE

		ld	hl, SRC_CHAR_PTR ;restore ptr to char gfx for loading
		ldi	a, [hl]
		ld	d, [hl]		; load 16 bit ptr from RAM
		ld	e, a		; de points to char gfx start again

		call 	LOAD_CHAR_TILE; load same tile again, de should still have ptr to char gfx
		ld 	a, [EMPTY]
		ld 	c, a
		call 	CHAR_TILE_SHL_C; chartile shl empty	
		call 	OUT_OR_CHAR
		call	COPY_OUT_TILE

		ld 	a,[CHAR_WIDTH]
		ld 	hl,EMPTY
		sub  	[hl] ;pos = charwidth-empty
		ld 	[POS],a


.popNLoop:
		
		call	CHECK_FIRST_LETTER

		ld	a, [LangId]
		res	7, a
		ld	[LangId], a
		pop	af
		ldh	[SVBK_WRAM_Bank], a ; restore Ram bank back
		ret
; End of function copyTileToVramBuffer


LOAD_CHAR_TILE:		;de points to char gfx
	
		ld 	hl, CHAR_TILE
		ld 	c, 8; one tile 8 bytes
.copy:
		ld 	a, [de]
		ldi      [hl], a
		inc     de
		dec     c 
		jr      nz, .copy
		ret

OUT_OR_CHAR: 
		ld 	b, 8
		ld 	hl, CHAR_TILE
		ld 	de, OUT_TILE
.loop:
		ld 	a, [de]
		or 	[hl]
		ld 	[de], a
		inc 	de
		inc 	hl
		dec 	b
		jr 	nz, .loop
	
		ret

CHAR_TILE_SHR_C:	;c has shift count

		ld 	a, c
		cp 	0
		jr 	z, .exit
	
		ld 	hl, CHAR_TILE
		ld 	b, 8 ; 8 bytes per tile
.shiftNext:
		ld 	d, c;restore shift count
		ld 	a, [hl]
.shiftScan:
		sra 	a
		and 	$7F;b7=b7, we need it to be 0
		dec 	d
		jr 	nz, .shiftScan
		ldi 	[hl], a
		dec 	b
		jr 	nz, .shiftNext
.exit:
		ret

CHAR_TILE_SHL_C:	;c has shift count
		ld 	a, c
		cp 	0
		jr 	z, .exit
		ld 	hl, CHAR_TILE
		ld 	b, 8 ; 8 bytes per tile
.shiftNext:
		ld 	d, c;restore shift count
.shiftScan:
		sla 	[hl]
		dec	d
		jr 	nz, .shiftScan
		inc 	hl
		dec 	b
		jr 	nz, .shiftNext
.exit:
		ret


COPY_OUT_TILE:	;hl points to outBuffer

		ld	a, [InvertPalFlag]
		ld	b, a

		ld	hl, OutBufferPtr
		ldi	a, [hl]
		ld	h, [hl]
		ld	l, a		; load in hl 16bit ptr on outBuffer from RAM

		ld 	de, OUT_TILE
		ld	c, 8		; 8 scanlines will be copied
.copyLoop:				
		ld	a, [de]		; load from ready tile
		cpl
		ldi	[hl], a		; save to RAM out buffer
		cpl			; xor FF
		bit	5, b		; if bit 5 is set, load	0
		jr	nz, .skipPalInvert ; load from DST to skip unused
					; 2nd bitplane (1bpp ->	2bpp)
		xor	a		; ld a,	0

.skipPalInvert:				
		ldi	[hl], a	; load from DST	to skip	unused
					; 2nd bitplane (1bpp ->	2bpp)

		inc	de
		dec	c
		jr	nz, .copyLoop	; load from font in ROM
		ret

CLEAR_OUT_TILE: 
		;zero outBuffer for future use
		ld 	de, OUT_TILE
		xor 	a
		ld	c, 8		; 8 scanlines will be copied
.copyLoop:
		ld 	[de], a 
		inc 	de
		dec 	c
		jr	nz, .copyLoop

		ret

CHECK_FIRST_LETTER: 			;first letter should be not shifted, catch that in name or menu render

		ld 	a, [SCREEN_POS]
		inc 	a
		and 	$0F
		ret 	nz		
		xor 	a		;if we're at the end of name string, reset position
		ld 	[POS], a
		call 	CLEAR_OUT_TILE
		ret

WIDTH_TBL:

DB  6,3,6,6,7,6,6,6,6,6;0-9
DB  6,6,6,6,5,5,7,6,4,7;A-J
DB  6,5,7,6,6,6,6,6,6,6;K-T
DB  6,6,8,6,6,6,9,3,8,6;U-..
DB  6,3,5,3,8,5,5,2,6,5;?-b
DB  5,6,5,6,5,5,2,5,5,2;c-l
DB  8,5,5,5,5,5,5,4,6,6;m-v
DB  8,6,5,5,8,8,8,8,8,8;w-end

DB  8,0,8,8,8,8,8,8,8,8,8,8,8,8,8,8;empty. 50 - full space; 51 - zero-width space for alignment

DB  6,6,6,5,7,5,6,6,6,6,6,6,6,6,6,6;�-�
DB  6,6,6,6,6,8,6,7,6,6,7,7,7,6,6,7;�-�
DB  6,6,5,5,5,5,5,5,8,6,6,6,5,6,6,5;�-�
DB  5,5,5,5,6,5,6,6,6,5,6,7,6,6,5,5;�-�
DB  7,5,7,7,8,8,8,8,8,8,8,8,8,8,8,8;�,�
DB  8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8;empty
DB  8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8;empty
DB  5,6,4,3,3,8,8,5,3,5,5,6,8,8,8,3;special

