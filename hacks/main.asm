	CPU GBZ80       ; switch to GBZ80 mode - tniAsm specific
;--------------------JUMPS-----------------------------
;jmp to copyTileHack
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x14075f ;paste compiled jump to ROM
	org	$475f, $47b5  ; original copyTile procedure here
	call 	copyTileHack ;jmp to our hack place
	ret	
	ds  	 47b6h-$, $FF; pad with FFs all not used code in original function 

;jmp to processSpecialCharHack
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x1407e1 ;paste compiled jump to ROM
	org	$47e1, $47ee  ; original processSpecialChar procedure here
	call 	processSpecialCharHack ;jmp to our hack place
	ret	
	ds  	 47efh-$, $FF; pad with FFs all not used code in original function 

;nullify position at item change
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x140239 ;paste compiled jump to ROM
	org	$4239, $423b ; original jp      loc_ROM50_4290
	jp 	resetPosOnItem ;jmp to our hack place

;nullify position at menu draw
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x144659 ;paste compiled jump to ROM
	org	$4659, $465b ; original ROM51:4659 C3 E0 10                    jp      nullify_and_ret
	jp 	resetPosOnMenu ;jmp to our hack place
	
;nullify position at name draw in dialog
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x14012c ;paste compiled jump to ROM
	org	$412c, $412d ; original ROM50:412C 7B 41                       dw renderNameString
	DW resetPosOnName ;define ptr to jump to our hack in a first place	

;render russian retry box strings
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x144eb7 ;paste compiled jump to ROM
	org	$4eb7, $4ebb 
	jp 	drawRetryBox ;jmp to our hack place
	nop; for alignment with next instruction (useless though)
	
;align names-frequencies in codec with proper spaces strings
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x144cdd ;paste compiled jump to ROM
	org	$4cdd, $4ce2
	jp 	alignNameWithSpaces ;jmp to our hack place
	ds  	 4ce3h-$, $FF; pad with FFs all not used code in original function

;--------------------HACKS-----------------------------
;bank 50
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x142560 ;paste compiled hack directly to free space in ROM
	org	$6560, $68FF ;free space in ROM
;Initial variables for further code
SrcFontPtr: EQU $C439
LangId: EQU $C430
OutBufferPtr: EQU $C43D
SVBK_WRAM_Bank: EQU $FF70
InvertPalFlag: EQU $C432
ChineseFont: EQU $7800
SCREEN_POS: EQU $C442

;My vars
CHAR_TILE: EQU $CEE0 ;should be in wram bank 0, as it's reset externally
OUT_TILE: EQU CHAR_TILE+8
POS: EQU	CHAR_TILE+$10
CHAR_WIDTH: EQU CHAR_TILE+$11
EMPTY: EQU CHAR_TILE+$12
SRC_CHAR_PTR: EQU CHAR_TILE+$13



copyTileHack:
	include	"copyTileHack.asm"

processSpecialCharHack:
	include "processSpecialCharHack.asm"

resetPosOnItem:
	include "resetPosOnItem.asm"
	
resetPosOnName:
	include "resetPosOnName.asm"	

;bank 51
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x147BC0
	org	$7BC0, $7CBF;free space in ROM - after that used some strings translation free space
resetPosOnMenu:
	include "resetPosOnMenu.asm"
drawRetryBox:
	include "drawRetryBox.asm"
alignNameWithSpaces:
	include "alignNameWithSpaces.asm"

;--------------------FONT-----------------------------
	fname   "Metal Gear Solid (VWF Hack) [C][!].gbc", 0x142900 ;paste vwf font
	org $6900, $6FFF
	incbin	"font.til"



