	
	ld b, a	
	
	cp      $F0 		; check if char is special (>F0)
	ret     c               ; if not special char, exit

	cp 	$FE		;new screen
	jr z, RESET_POS
	cp 	$FD
	jr z, RESET_POS
	cp 	$F8		;new screen
	jr z, RESET_POS


	cp	$FF		; FF means end of namestring
	jr nz,	EXIT	
	ld 	a, [SCREEN_POS]
	cp 0
	jr nz, RESET_POS	; screenpos is used only in name render

EXIT:				;original code
	ld a, b			;if all checks failed, it's usual code, proceed to original calcs
	and     $F		
	ld      hl, $47ef	;call    jmpOnTableAt_hl ; hl-base, a-idx
	call    1 		;jump to further check
	scf
	ccf
	ret


RESET_POS:
	xor 	a
	ld 	[POS],a 	;reset position for new line
	ld 	hl, OUT_TILE
	ld	c, 8		; 8 scanlines will be cleared
CLEAR_LOOP:
	ldi 	[hl], a 
	dec 	c
	jr	nz, CLEAR_LOOP

	jp 	EXIT 		;also proceed to usual actions


